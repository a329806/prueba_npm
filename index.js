const log4js = require("log4js");

let logger = log4js.getLogger();

logger.level="debug"; // production

logger.info("La aplicacion se ejecuto con exito!");
logger.warn("Cuidado falta la libreria X en el sistema!!!");
logger.error("(1) No se encontro la funcion: Enviar Email.");
logger.fatal("No se pudo iniciar la aplicacion.");

function sumar(x, y) {
  return x + y;
}

module.exports = sumar;
