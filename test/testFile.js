const sumar = require("../index");
const assert = require("assert");
// Asserts - frase logica que describe un caso de prueba (afirmacion)

describe("Probar la suma de dos numeros", () => {
  // afirmamos que cinco mas siete es 12
  it("Cinco mas siete son 12", () => {
    assert.equal(12, sumar(5, 7));
  });
  // afirmamos que cinco mas siete no son 57
  it("Cinco mas siete no son 57", () => {
    assert.notEqual("57", sumar(5, 7));
  })
});
